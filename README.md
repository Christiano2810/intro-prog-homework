# LU 1 - Variables
#### LU1 - Exercise 1
``` py
stock_fav = "TSLA"
stock_value = 321.01
stock_tradevolume = 20
stock_action = "buy"
print("I would like to", stock_action,stock_tradevolume, "shares in", stock_fav)
```

#### LU1 - Exercise 2
``` py
person1 = "Chris"
person2 = "Cesco"
x = 10
y = 1
companyz = 'APPL'
print(person1, "needs to borrow", x , "euros from", person2, "in order to trade", y, "stocks in", companyz)
```

#### LU1 - Exercise 3
``` py
a = 2
b = 3
result_ex3 = a*b
print(result_ex3)
``` 

#### LU1 - Exercise 4
``` py
celsius = 30
kelvin = celsius * 9 / 5 + 32
fahrenheit = celsius + 273.15
print("degrees in celsius", celsius, "in kelvin", kelvin, "in fahrenheit", fahrenheit)
```



# LU 2 - FUNCTIONS
#### LU2 - Exercise 1
``` py
def power(a,b):       # name and inputs
    ret_val = a ** b  # here we create a variable that takes certain values, however the step is not neccessary
    return ret_val    # this is the output, this is what our function does

a=2                   # here we define the inputs
b=4
print(power(a,b))
```

#### LU2 - Exercise 2
``` py
def function_equal(value,testvalue):
    return value==testvalue

value=2
testvalue=2
print(function_equal(value,testvalue))
```
#### LU2 - Exercise 3
``` py
def function_pyt(o,p):    
    c_sq= o ** 2 + p ** 2
    c = c_sq **(1/2)
    return c

o=2
p=2
function_pyt(o,p)
```

#### LU2 - Exercise 4
``` py
a = 4
def function_multiply():
    return a*2

print(a,function_multiply())
#or just
function_multiply()
```



# LU 3 - Keyword Arguments & Scope

#### LU3 - Exercise 4
``` py
e = 4
def function_multiply(e):
     return e*2
result = function_multiply(e)
print(e,result)
```

#### LU3 - Exercise 11
``` py
def greetings(message='Good', name="morning"):
    print(message, name)

greetings(name='John Snow', message='You know nothing')
```




# LU 4 - Data Structures
#### LU4 - Exercise 0
``` py
first_tuple = ('a','b','c')
first_tuple[0]
first_tuple[1]
first_tuple[2]
```

#### LU4 - Exercise 4 - list basics, add, sort
``` py
apple_stock_list = [140,120,160,185,180]

apple_stock_list.append(190)
apple_stock_list.sort() ## ascending
apple_stock_list.sort(reverse=True) ## descending
```

#### LU4 - Exercise 7 - turn into list
``` py
tuple = (1, 2, 3)
new_list = list(tuple)
```

#### LU4 - Exercise 9 - empty dictionary
``` py
empty_dict = {}
empty_dict['hello'] = 'World'
empty_dict['age'] = 25

empty_dict.keys() # hello, age
empty_dict.values() # world, 25
empty_dict.items() # hello, world & age,25
```

#### LU4 - Exercise 11 - changing data structures
``` py
# Lists
list_one = []
list_two = list()

# dictionaries
dict_one = {}
dict_two = dict()

# tuples
t_one = ()
t_two = tuple()
```

#### LU4 - Exercise 12 - removing keys from dictionary 
``` py
first_dict = {'a':1, 'b':2, 'hallo':'Welt'}
first_dict.pop('a')
del first_dict['b']
```

#### LU4 - Exercise 13 - adding to list
``` py
def add_homie(a_list):
    a_list.append('homie')
    return a_list
print (add_homie([1,2,3]))
```

#### LU4 - Exercise 14 - summing lists
``` py
stock_GOOG=100
stock_APPL=100
stock_KMPG=200
stock_BCG=200
stock_MSFT=100
stock_OSYS=100
stock_PHZR=200

def function_sum(stock_GOOG,stock_APPL,stock_KMPG,stock_BCG,stock_MSFT,stock_OSYS,stock_PHZR):
    a = sum([stock_GOOG,stock_APPL,stock_KMPG,stock_BCG,stock_MSFT,stock_OSYS,stock_PHZR])/7
    return a

function_sum(stock_GOOG,stock_APPL,stock_KMPG,stock_BCG,stock_MSFT,stock_OSYS,stock_PHZR)

def sum_stocks(list_stocks):
    return sum(list_stocks)  
```


# LU 5 - Flow Control
#### LU5 - Exercise 2 - True or False
``` py
def function_ticker(ticker):
    if ticker == 'APPL':
        return True
    else:
        return False
```

#### LU5 - Exercise 3 
``` py
gender = 'male'

def gender_color(gender):
    if gender == "male":
        return "you like pink"
    
    if gender == "female":
        return "you hate pink"

gender_color(gender)
```

#### LU5 - Exercise 5
``` py
def function_age(age_mom, age_dad):
    if age_mom == age_dad:
        return age_mom + age_dad
    else: 
        return age_mom - age_dad

print(function_age(60,60))
```

#### LU5 - Exercise 6
``` py
dict_test = {1,2,3,4}
list_test = ["a","b","c","d"]

def function_test(dict_test,list_test):
    if len(dict_test) == len(list_test):
        return('awesome')
    else:
        return('that is sad')
        
function_test(dict_test,list_test)
```

#### LU5 - Exercise 7
``` py
dict_stocks = {
        'APPL': 100,
        'GOOG': 200,
        'FB': 50,
        }

for key in dict_stocks:
    print(key)
```    
    

#### LU5 - Exercise 8
``` py
tuple_3 = (10,20,30)
list_3 = [40,50,60]
dict_3 = {
        'APPL': 100,
        'GOOG': 90,
        'FB': 80
        }
    
for num in tuple_3:
    print(num)

for num in list_3:
    print(num)
    
for key in dict_3:
    print('key', key, 'value', dict_3[key])
```    
    

#### LU5 - Exercise 9
``` py
list_ex9 = [1,2,3,4]

def function_multiply(list_ex9):
    list_multiplied = []
    
    for x in list_ex9:
        x_multiplied = x * 2
        list_multiplied.append(x_multiplied) 
        
    return list_multiplied

print(function_multiply(list_ex9))
```
2nd approach
``` py
def function_9(a_list=[1,2,3]):
    new_list = [num*2 for num in a_list]
    return new_list

function_9(a_list=[1,2,3])
```


#### LU5 - Exercise 10
``` py
my_dict =   {'APPL':100,
             'GOOG': 90,
             'FB': 80}

def dict_multiplier(my_dict):
    dict_doubled = {}
    
    for key in my_dict:
        dict_doubled[key] = my_dict[key]*2 
    return dict_doubled 
    
print(dict_multiplier(my_dict))
```

#### LU5 - Exercise 11
``` py
a_list = [1,2,3,4]
a_number = 123

def function_length(a_list,a_number):
    if len(str(a_number)) < len(a_list):
        return True
    else:
        return False

print(function_length(a_list,a_number))
```  


    